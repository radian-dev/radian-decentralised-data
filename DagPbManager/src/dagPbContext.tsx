import { createContext } from 'react';

const DagPbContext = createContext(null);

export default DagPbContext;
