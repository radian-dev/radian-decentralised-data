import "./styles/app.less";
import { useState, useContext, useRef, useEffect } from "react";

import { WebBundlr } from "@bundlr-network/client"
import BigNumber from "bignumber.js";
import { Button, Input, message } from "antd";

import WalletConnectProvider from "@walletconnect/web3-provider";
import { providers } from "ethers"
import { Web3Provider } from "@ethersproject/providers";
//import { PhantomWalletAdapter } from "@solana/wallet-adapter-phantom"

import DagPbContext from "./dagPbContext";

declare var window: any // TODO: specifically extend type to valid injected objects.
const PhantomWalletAdapter = require("@solana/wallet-adapter-phantom/lib/cjs/index").PhantomWalletAdapter

const ipfsNodeUrl: string = "https://ipfs.gateway.radian.community";
const bundlerUrl: string = "https://node1.bundlr.network";

function App() {

  const defaultCurrency = "matic"
  const defaultSelection = "Select a Provider"

  const [currency, setCurrency] = useState<string>(defaultCurrency);
  const [address, setAddress] = useState<string>();
  const [selection, setSelection] = useState<string>(defaultSelection);
  const [balance, setBalance] = useState<string>();
  const [img, setImg] = useState<Buffer>();
  const [price, setPrice] = useState<BigNumber>();
  const [bundler, setBundler] = useState<WebBundlr>();
  const [bundlerHttpAddress, setBundlerAddress] = useState<string>(bundlerUrl);
  const [fundAmount, setFundingAmount] = useState<string>();
  const [cidHash, setCidHash] = useState<string>("QmaZuKKb4vpddBcFXQdAUovmLwwb7YRHFkEqvaX3syj2zb");
  const [cidPath, setCidPath] = useState<string>("root");
  const [newData, setNewData] = useState<string>(null);
  const [newFileName, setNewFileName] = useState<string>("newFile.json");
  const [provider, setProvider] = useState<Web3Provider>();

  const [currentNode, setCurrentNode] = useState<any>(null);

  const [arTxList, setArTxList] = useState<any>(null);

  const cidObj = useContext(DagPbContext);

  useEffect(()=>{
    // initialise provider with IPFS url and root cidHash
    cidObj.init(cidHash, ipfsNodeUrl);
    // TODO see if need to put cidHash as dependency
    // as the root cid hash may change overtime, as user decides to add more file to the tree
  }, [cidHash, ipfsNodeUrl])

  const intervalRef = useRef<number>();

  const clean = async () => {
    clearInterval(intervalRef.current)
    setBalance(undefined);
    setImg(undefined);
    setPrice(undefined);
    setBundler(undefined);
    setProvider(undefined);
    setAddress(undefined);
    setCurrency(defaultCurrency);
    setSelection(defaultSelection);
  }

  // const handleFileClick = () => {
  //   var fileInputEl = document.createElement("input");
  //   fileInputEl.type = "file";
  //   fileInputEl.accept = "image/*";
  //   fileInputEl.style.display = "none";
  //   document.body.appendChild(fileInputEl);
  //   fileInputEl.addEventListener("input", function (e) {
  //     handleUpload(e as any);
  //     document.body.removeChild(fileInputEl);
  //   });
  //   fileInputEl.click();
  // };

  // const handleUpload = async (evt: React.ChangeEvent<HTMLInputElement>) => {
  //   let files = evt.target.files;
  //   let reader = new FileReader();
  //   if (files && files.length > 0) {
  //     reader.onload = function () {
  //       if (reader.result) {
  //         setImg(Buffer.from(reader.result as ArrayBuffer));
  //       }
  //     };
  //     reader.readAsArrayBuffer(files[0]);
  //   }
  // };

  // top up fund to bundlr account
  const fund = async () => {
    if (bundler && fundAmount) {
      message.info("Funding...", 15000);
      const value = parseInput(fundAmount)
      if (!value) return
      await bundler.fund(value)
        .then(res => { message.success(`Funded ${res?.target}. tx ID : ${res?.id}`, 10000) })
        .catch(e => {
          message.error(`Error: Failed to fund - ${e.data?.message || e.message}`, 10000)
        })
    }
  };

  const setNode = (node: any)=> {
    if (node) {
      let newNode: any = [];
      node.Links.forEach((d: any)=>{
        newNode.push({...d, Hash: d.Hash.toString()});
      })
      setCurrentNode(newNode);
    }
  }

  const displayData = async () => {
    const [,,node] = await cidObj.advanceCursor(cidPath);
    setNode(node);
  }

  const insertData = async () => {    
    const cid = await cidObj.addPbFile(
      (cidPath + "/" + newFileName).replace("//", "/"),
      newData
    )
    // trasverse to directory containing the file
    const [subroot,, node] = await cidObj.advanceCursor(cidPath);
    setNode(node);
    setCidHash(cid);
    setCidPath(subroot);
  }

  const goBackRoute = async () => {
    const [subroot,, node] = await cidObj.fallbackCursor();
    console.log(subroot, node);
    setNode(node);
    setCidPath(subroot);
  }

  // prepare cid for upload
  const prepareCID = async () => {
    const arTxList = await cidObj.getARData(bundler, currency);
    setArTxList(arTxList);
  }

  // upload files to arweave one-by-one
  const uploadFile = async () => {
    if (cidObj.nextArTx()) {
      const arid = await cidObj.signArTx(bundler);
      message.success("Success: " + `https://arweave.net/${arid}`, 15000);
    }
    if (! cidObj.nextArTx) {
      message.success("Success: All Upload Completed", 15000);  
    }
  };

  // field change event handlers
  const updateAddress = (evt: React.BaseSyntheticEvent) => {
    setBundlerAddress(evt.target.value);
  };

  const updateFundAmount = (evt: React.BaseSyntheticEvent) => {
    setFundingAmount(evt.target.value);
  };

  const updateCidHash = (evt: React.BaseSyntheticEvent) => {
    setCidHash(evt.target.value);
  };

  const updatePath = (evt: React.BaseSyntheticEvent) => {
    setCidPath(evt.target.value);
  };

  const updateFileName = (evt: React.BaseSyntheticEvent) => {
    setNewFileName(evt.target.value);
  };

  const updateData = (evt: React.BaseSyntheticEvent) => {
    setNewData(evt.target.value);
  };

  const connectWeb3 = async (connector: any) => {
    if (provider) {
      await clean();
    }
    const p = new providers.Web3Provider(connector);
    await p._ready();
    return p
  }

  /**
   * Map of providers with initialisation code - c is the configuration object from currencyMap
   */
  const providerMap = {
    "MetaMask": async (c: any) => {
      if (!window?.ethereum?.isMetaMask) return;
      await window.ethereum.enable();
      const provider = await connectWeb3(window.ethereum);
      const chainId = `0x${c.chainId.toString(16)}`
      try { // additional logic for requesting a chain switch and conditional chain add.
        await window.ethereum.request({
          method: 'wallet_switchEthereumChain',
          params: [{ chainId }],
        })
      } catch (e: any) {
        if (e.code === 4902) {
          await window.ethereum.request({
            method: 'wallet_addEthereumChain',
            params: [{
              chainId, rpcUrls: c.rpcUrls, chainName: c.chainName
            }],
          });
        }
      }
      return provider;
    },
    "WalletConnect": async (c: any) => { return await connectWeb3(await (new WalletConnectProvider(c)).enable()) },
    "Phantom": async (c: any) => {
      if (window.solana.isPhantom) {
        await window.solana.connect();
        const p = new PhantomWalletAdapter()
        await p.connect()
        return p;
      }
    }
  } as any

  const ethProviders = ["MetaMask", "WalletConnect"]

  const currencyMap = {
    "solana": {
      providers: ["Phantom"], opts: {}
    },
    "matic": {
      providers: ethProviders,
      opts: {
        chainId: 137,
        chainName: 'Polygon Mainnet',
        rpcUrls: ["https://polygon-rpc.com"],
      },
    }
  } as any


  /**
   * initialises the selected provider/currency
   * @param cname currency name
   * @param pname provider name
   * @returns 
   */
  const initProvider = async () => {
    if (intervalRef.current) {
      clearInterval(intervalRef.current)
    }
    if (provider) {
      setProvider(undefined);
      setBundler(undefined);
      setAddress(undefined);
      return;
    }

    const pname = "MetaMask" as string;
    const cname = currency as string;
    const p = providerMap[pname] // get provider entry
    const c = currencyMap[cname]
    console.log(`loading: ${pname} for ${cname}`)
    const providerInstance = await p(c.opts).catch((e: Error) => { 
      message.error(`Failed to load provider ${pname}`, 10000); console.log(e); return; })
    setProvider(providerInstance)
  };

  const initBundlr = async () => {
    const bundlr = new WebBundlr(bundlerHttpAddress, currency, provider)
    try {
      // Check for valid bundlr node
      await bundlr.utils.getBundlerAddress(currency)
    } catch {
      message.error(`Failed to connect to bundlr ${bundlerHttpAddress}`, 10000)
      return;
    }
    try {
      await bundlr.ready();
    } catch (err) {
      console.log(err);
    } //@ts-ignore
    if (!bundlr.address) {
      console.log("something went wrong");
    }
    message.success(`Connected to ${bundlerHttpAddress}`, 10000)
    setAddress(bundlr?.address)
    setBundler(bundlr);
  }

  const toProperCase = (s: string) => { return s.charAt(0).toUpperCase() + s.substring(1).toLowerCase() }
  const toggleRefresh = async () => {
    if (intervalRef) {
      clearInterval(intervalRef.current)
    }

    intervalRef.current = window.setInterval(async () => { bundler?.getLoadedBalance().then((r) => { setBalance(r.toString()) }).catch(_ => clearInterval(intervalRef.current)) }, 5000)
  }

  // parse decimal input into atomic units
  const parseInput = (input: string | number) => {
    const conv = new BigNumber(input).multipliedBy(bundler!.currencyConfig.base[1]);
    if (conv.isLessThan(1)) {
      message.error("Value too small!")
      return;
    }
    return conv;
  }
  
  return (
    <div>
      <Button onClick={async () => await initProvider()}>
        {provider ? "Disconnect" : "Connect"}
      </Button>
      <div>Connected Account: {address ?? "None"}</div>
      <div>
        <Button disabled={!provider} onClick={async () => await initBundlr()}>
          Connect to Bundlr
        </Button>
        <Input
          value={bundlerHttpAddress}
          onChange={updateAddress}
          placeholder="Bundler Address"
        />
      </div>
      {
        bundler && (
          <>
            <div>
              <Button
                onClick={async () => {
                  address &&
                    bundler!
                      .getBalance(address)
                      .then((res: BigNumber) => {
                        setBalance(res.toString())
                      });
                  await toggleRefresh();
                }}

              >
                Get {toProperCase(currency)} Balance
              </Button>
              {balance && (
                  <div>
                    {toProperCase(currency)} Balance: {bundler.utils.unitConverter(balance).toFixed(7, 2).toString()} {bundler.currencyConfig.ticker.toLowerCase()}
                  </div>
              )}
            </div>

            <div>
              <Button onClick={fund}>
                Fund Bundlr
              </Button>
              <Input
                placeholder={`${toProperCase(currency)} Amount`}
                value={fundAmount}
                onChange={updateFundAmount}
              />
            </div>
            <div>
              <span>
                <Button  onClick={displayData}>
                  Search Path
                </Button>
                <Button  onClick={goBackRoute}>
                    Back
                </Button>
                {currentNode && 
                  <Button  onClick={insertData}>
                    Insert Data
                  </Button>
                }
                <Button  onClick={prepareCID}>
                  Prepare CID For Upload
                </Button>
              </span>
              <Input
                addonBefore="Root CID: "
                placeholder={`CID`}
                value={cidHash}
                onChange={updateCidHash}
              />
              <Input
                addonBefore="PATH: "
                placeholder={`Path`}
                value={cidPath}
                onChange={updatePath}
              />
              {currentNode &&
              <span>
                <Input
                  addonBefore="NewFileName: "
                  placeholder={`FileName`}
                  value={newFileName}
                  onChange={updateFileName}
                />
                <Input
                  addonBefore="NewData: "
                  placeholder={`Data`}
                  value={newData}
                  onChange={updateData}
                />
              </span>
              }
            </div>
            {currentNode && ( currentNode.map((d:any) => {
              return <div>{JSON.stringify(d)}</div>
              })
            )}
            {
              arTxList && (
                <div>
                  <div>
                    {/* <Button onClick={handlePrice}>Get Price</Button> */}
                    {cidObj.nextArTx() && (
                      <div>
                        <div>{`Uploading: ${cidObj.nextArTx().path}`}</div>
                        <div>{`Upload Data Size: ${cidObj.nextArTx().size}`}</div>
                        <div>{`Cost: ${bundler.utils.unitConverter(cidObj.nextArTx().price).toString()} ${bundler.currencyConfig.ticker.toLowerCase()} `}</div>
                        <Button onClick={uploadFile}>Upload to Bundlr Network</Button>
                      </div>
                    )}
                  </div>
                </div>
              )
            }
          </>
        )
      }
    </div>
  );
}

export default App;
