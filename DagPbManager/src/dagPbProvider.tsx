// manage the dag-pb directory and the uplaod of IPFS cids to Arweave
import { create, IPFSHTTPClient } from 'ipfs-http-client';
import { CID } from 'multiformats/cid';
import * as FileType from 'file-type/browser';

import {useState , FC} from "react";
import DagPbContext from "./dagPbContext"

interface Link {
    Hash: CID,
    Name: string,
    Tsize: number
}

const DagPbProvider: FC = ({ children }) => {

    let [ipfs, setIpfs] = useState<IPFSHTTPClient>(null);
    let [cursor, setCursor] = useState<string>("root");
    let [rootCid, setRootCid] = useState<string>(null);
    let [cache, setCache] = useState<any>({});
    let [arTxList, setArTxList] = useState<any>([]);

    // initialise the provider, used with useEffect
    // all dag-pb directory starts with name root
    const init = async (rootCid: string, ipfs: string | IPFSHTTPClient) => {
        
        let ipfs_node: IPFSHTTPClient;
        if ( (typeof ipfs) == "string") {
            ipfs_node = create({url: ipfs as string});
        } else {
            ipfs_node = ipfs as IPFSHTTPClient;
        }
        cache['root'] = (await ipfs_node.dag.get(CID.parse(rootCid))).value;
        setCache(cache);
        setIpfs(ipfs_node);
        setRootCid(rootCid);
        setCursor("root");
    };

    // advance to the given path, path needs to be absolute path
    const moveCursor = async (path: string) => {
        let [subRoot, subLeave, node] = await traverse(path);
        setCursor(subRoot);
        return [subRoot, subLeave, node];
    }

    // advance to the given path, accept both relative or absolute input
    const advanceCursor = async (givenPath: string) => {
        // remove front or end /
        givenPath = givenPath[0] == "/" ? givenPath.slice(1,givenPath.length) : givenPath;
        givenPath = givenPath[givenPath.length-1] == "/" ? givenPath.slice(0,givenPath.length-1) : givenPath;
        const path = givenPath.slice(0,4) == "root" ? givenPath : cursor + "/" + givenPath;
        return await moveCursor(path);
    }

    // go back one directory
    const fallbackCursor = async (times=1) => {
        const pathList = cursor.split("/")
        if (pathList.length <= times) {
            return [cursor, "", node()];
        }
        const path = pathList.slice(0, pathList.length-times).join("/")
        return await moveCursor(path);
    }

    // return the current node
    const node = () => {
        return cache[cursor];
    }

    // add a signle file to IPFS
    const addIPFSContent = async (files: any) => {
        let links = [];
        for await (const result of ipfs.addAll(files)) {
            links.push({Hash: result.cid, Name: result.path, Tsize: result.size})
        }
        return links;
    }

    // get node from IPFS
    const getPbNode = async (cid: CID | string)=>{
        cid = (typeof cid) == "string" ? CID.parse(cid as string) : cid;
        return (await ipfs.dag.get(cid as CID)).value;
    }

    // upload a node to IPFS, get the stats of the upload and return data
    const putPbNode = async (pbNode: any, Name="") => {
        const result = await ipfs.dag.put(pbNode, { storeCodec: 'dag-pb', hashAlg: 'sha2-256' });
        const cid = result.toV0();
        // grab details of newly created node
        const details = (await ipfs.files.stat('/ipfs/' + cid));
        // return link obj
        return {
            Hash:  cid,
            Name:  Name,
            Tsize: details.cumulativeSize
        }    
    }

    // split the string by the n-th "/"
    const splitAt = (path: string, at: number) => {
        const splitPath = path.split("/")
        return [splitPath.slice(0, at).join("/"), splitPath.slice(at, splitPath.length).join("/")]
    }

    // This function traverse down the tree with a given path, until it can no longer go further. 
    // The intermediate nodes are cached 
    // return [ the path to leave node, the path from leave node to desired node, the current node ]
    const traverse = async (fullPath: string) => {

        const pathList = fullPath.split("/");

        // transverse  pbNode to find leaf
        for ( let i = 0 ;  i < pathList.length ; i++ ) {
            
            // check if the node is already cached
            const currentPath = splitAt(fullPath, i+1)[0]
            if (cache[currentPath] != undefined) {
                continue
            }

            // if not cached, check the parent node for its cid hash, and fetch the data from ipfs
            if (i > 0) {
                const parentPath = splitAt(fullPath, i)[0]
                const parentNode = cache[parentPath]
                for (let l = 0 ; l < parentNode.Links.length ; l++ ) {
                    if (parentNode.Links[l].Name == pathList[i]) {
                        cache[currentPath] = await getPbNode(parentNode.Links[l].Hash);
                        break;
                    }
                }
            }

            // leave loop if no subnode is found
            if (cache[currentPath] == null || cache[currentPath].Links.length == 0) {
                let [subRoot, subLeave] = splitAt(fullPath, i);
                setCache(cache);
                return [subRoot, subLeave, cache[subRoot]];
            }
        }
        setCache(cache);
        return [fullPath, "", cache[fullPath]];
    }

    // compare the two strings to see which one is first
    const linkComparator = (a: any, b: any) => {
        if (a === b) {
            return 0;
        }
        const abuf: any = a.Name ? new TextEncoder().encode(a.Name) : [];
        const bbuf: any = b.Name ? new TextEncoder().encode(b.Name) : [];
        let x = abuf.length;
        let y = bbuf.length;
        for (let i = 0, len = Math.min(x, y); i < len; ++i) {
            if (abuf[i] !== bbuf[i]) {
            x = abuf[i];
            y = bbuf[i];
            break;
            }
        }
        return x < y;
    }

    // add a link to a dag-Pb node, the links are required to be arranged in alphbetical order
    // links with same name are updated, whereas non-existing links are inserted
    const addLink = (node: any, link: any) => {
        // remove existing link
        for ( let l = 0 ; l < node.Links.length ; l++ ) {
            if (node.Links[l].Name == link.Name ) {
                node.Links.splice(l, 1);
                break
            }
        }
        // getBytes, insert link based on the decending order of Name Bytes
        for ( let l = 0 ; l < node.Links.length; l++ ) {
            if ( linkComparator(link, node.Links[l]) && ( l > 0 && linkComparator(node.Links[l-1], link) || l == 0 ) ) {
                node.Links.splice(l, 0, link);
                return node;
            }
        }
        node.Links.push(link);
        return node;
    }

    // add a file to a particular dag-pb directory, and re-upload all the parent/grandparent etc nodes of that file
    // a new root cid hash is returned and is updated
    const addPbFile = async (givenPath: string, content: any) => {

        const path = givenPath.slice(0,4) == "root" ? givenPath : cursor + "/" + givenPath;
        let [subRoot, subLeave, node] = await traverse(path);

        // create file
        const files = 
        [{
            path: "/" + subLeave,
            content: content
        }]
        
        // create file with path on IPFS
        const linkList = await addIPFSContent(files);

        // add fileLink to pbNode
        const subRootList = subRoot.split("/");

        // upload all subRoots as a list of files
        for (let i = subRootList.length-1 ; i >= 0 ; i--) {
            
            // merge link to node
            let currentNode = cache[splitAt(subRoot, i+1)[0]];
            currentNode = addLink(currentNode, linkList[linkList.length - 1]);

            // upload subnode
            const newLink = await putPbNode(currentNode, subRootList[i]);
            linkList.push(newLink);
        }

        rootCid = linkList[linkList.length - 1].Hash.toString();
        setRootCid(rootCid); // update rootCid
        return rootCid;
    }

    // recursively search the dagPb tree for all nodes, append data to memory along the way
    const unwrapCID = async (link: Link, ipfs: IPFSHTTPClient, dataDict: any, pathDict: any, currentPath: Array<string>) => {
    
        // get cached node from cache
        let newPath = [...currentPath];
        newPath.push(link.Name);
        let cachedNode = cache[newPath.join("/").slice(1,newPath.join("/").length)]
        let node = cachedNode != undefined ? cachedNode : await getPbNode(link.Hash);
        // if links == 0 means leaf node, append data, else continue recursion
        if ( node.Links.length == 0 ) {
          dataDict[link.Hash.toString()] = {Link: link, Data: node.Data, Path: newPath.join("/")};
          pathDict['path'][newPath.join("/")] = link.Hash.toString();
          return;
        }    
        for ( let i = 0 ; i < node.Links.length ; i ++ ) {
            await unwrapCID(node.Links[i], ipfs, dataDict, pathDict, newPath);
        }
    }

    // get the mime type from the data buffer, default to text/plain
    const getFileType = async (dataBuffer: any)=>{
        const fileTypeObj = await FileType.fromBuffer(dataBuffer);
        let mimeType;
        if ( fileTypeObj == undefined ) {
          // try parse as Json
          try {
            const dataString = new TextDecoder().decode(dataBuffer);
            const dataJSON = JSON.parse(dataString);
            if (dataJSON) {
              mimeType = "application/json";
            }
          } catch (e) { 
            console.log(e);
            mimeType = "text/plain"
          }
        } else {
          mimeType = fileTypeObj.mime;
        }
        return mimeType;
    }

    // back up to arweave of the root hash
    // requires a bunldr object that is already initialised with provider
    // tags are not provided as content-type as Arweave tends to re-compress jpg file on upload
    // re-compressed jpg cannot be recovered into the original cid hash
    const getARData = async (bundler: any, currency: string) => {

        arTxList = [];
        let tx: any;

        // parse cid into subcomponents
        let rootData = cache["root"];
        let dataDict: any = {};
        let pathDict: any = {"path": {"/root": rootCid}, "data": {[rootCid]: "self.id"}};
        let currentPath = ["/root"];
        for ( let i = 0 ; i < rootData.Links.length ; i ++ ) {
            await unwrapCID(rootData.Links[i], ipfs, dataDict, pathDict, currentPath);
        }

        // if it is not a dag-pb directory
        if (rootData.Links.length == 0) {
            const dataBuffer = unpad(rootData.Data);
            const details = (await ipfs.files.stat('/ipfs/' + rootCid));
            const dataTags = [
              {name: "Tsize", value: details.cumulativeSize.toString()},
              {name: "Hash", value: rootCid}, 
              {name: "Content", value: await getFileType(dataBuffer)}
            ];            
            // create arweave transaction
            tx = bundler.createTransaction(dataBuffer, { tags: dataTags } );
            const price = await bundler?.utils.getPrice(currency as string, tx.size);
            arTxList.push({size: tx.size, price: price, path: "data", data: dataBuffer, tags: dataTags, hash: rootCid});            
            return arTxList;
        }

        let totalSize = 0;
        for ( let cid in dataDict) {
          const data = dataDict[cid];
          const dataBuffer = unpad(data.Data);      
          const dataTags = [
            {name: "Tsize", value: data.Link.Tsize.toString()},
            {name: "Hash", value: data.Link.Hash.toString()},
            {name: "Content", value: await getFileType(dataBuffer)}
          ];
          totalSize += dataBuffer.length;
          
          // create arweave transaction
          tx = bundler.createTransaction(dataBuffer, { tags: dataTags } );
          const price = await bundler?.utils.getPrice(currency as string, tx.size);
          const name = data.Path;
          arTxList.push({size: tx.size, price: price, path: name, data: dataBuffer, tags: dataTags, hash: data.Link.Hash.toString()});
      }

      // finally add in the root dict
      const dataTags = [
        {name: "Tsize", value: totalSize.toString()},
        {name: "Hash", value: rootCid}, 
        {name: "Content", value: "application/json"}
      ];

      tx = bundler.createTransaction(JSON.stringify(pathDict), { tags: dataTags } );
      const price = await bundler?.utils.getPrice(currency as string, tx.size);
      // the pathDict is stored as JSON instead of buffer because we need to add in the arid of the former transactions
      arTxList.push({size: tx.size, price: price, path: "root", data: pathDict, tags: dataTags});
      setArTxList(arTxList);

      return arTxList;
    }

    // receive the information of the next batch of data to be uploaded to Arweave
    const nextArTx = () => {
        return arTxList.length > 0 ? arTxList[0] : null;
    }

    // sign and upload data to Arweave
    const signArTx = async (bundler: any)=>{
        if (arTxList.length == 0) return;
        if (arTxList.length == 1 && arTxList[0].path == 'root') {
            arTxList[0].data = JSON.stringify(arTxList[0].data);
        }
        console.log(arTxList[0].path, arTxList[0].data, arTxList[0].tags);
        let tx = bundler.createTransaction(arTxList[0].data, { tags: arTxList[0].tags } );
        await tx.sign();
        const res = await tx.upload();
        let arid = res.data.id;

        if ( arTxList.length > 1) {
            arTxList[arTxList.length-1].data["data"][arTxList[0].hash] = arid;
        }
        setArTxList(arTxList.slice(1,arTxList.length));
        return arid;
    }

    // compare two arrays of databuffers
    const arrayCmp = (a1: any, a2: any) => {
        for (let i = 0 ; i < a1.length ; i ++ ) {
          if (a1[i] != a2[i]) {
            return false;
          }
        }
        return true;
      }
      
      // remove the added bytes for data correction, bytes added format: [3 bytes + svBytes + {original data} + 1 byte + svBytes ]
      // svBytes have the same values, so we check and remove identical bytes
      const unpad = (dataBuffer: any)=>{
        if (dataBuffer.length < 6) {
          console.log("Error: no data");
          return new Uint8Array();
        }
        let count = 1;
        while (true) {
          if ( arrayCmp(dataBuffer.slice(3, 3+count), dataBuffer.slice(dataBuffer.length-count, dataBuffer.length) )) {
            return dataBuffer.slice(3+count, dataBuffer.length-count-1);
          }
          if (count == 10 ) {
            // raise error
            console.log("Error: sv too large");
            return new Uint8Array();
          }
          count++
        }
    }

    const providerValue = {
        // states
        cursor: cursor,
        cache: cache,
        rootCid: rootCid,
        ipfs: ipfs,
        arTxList: arTxList,
        // for search and traverse pBtree
        node: node,
        init: init,
        moveCursor: moveCursor,
        advanceCursor: advanceCursor,
        fallbackCursor: fallbackCursor,
        // for get and set data at IPFS node
        getPbNode: getPbNode,
        addPbFile: addPbFile,
        // for upload data to Arweave
        getARData: getARData,
        nextArTx: nextArTx,
        signArTx: signArTx
    };

    return (<DagPbContext.Provider value={providerValue}>
                {children}
            </DagPbContext.Provider>)

}

export default DagPbProvider;