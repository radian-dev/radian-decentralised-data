import json
import io
import base64
from cid import make_cid

import ipfshttpclient
import arweave
from arweave.arweave_lib import Transaction


txid = "QhlEKjzy6NbviRyS7KDXEmW1_6gdYK_vEjWk9Hx0pHY"

pre = b'\x08\x02\x12'
post = b'\x18'
# pre = (8).to_bytes(1, 'little') + (2).to_bytes(1, 'little') + (18).to_bytes(1, 'little')
# post = (24).to_bytes(1, 'little')

def _byte(b):
    return bytes((b, ))

def encode(number):
    """Pack `number` into varint bytes"""
    buf = b''
    while True:
        towrite = number & 0x7f
        number >>= 7
        if number:
            buf += _byte(towrite | 0x80)
        else:
            buf += _byte(towrite)
            break
    return buf

# read the data from the root file of arweave
client = ipfshttpclient.connect('/dns/ipfs.gateway.radian.community/tcp/443/https')
wallet = arweave.Wallet("./arweave-wallet.json")
tx = Transaction(wallet, id=txid)
tx.get_data()

directory_data = json.loads(tx.data.decode('utf-8'))
# get the root ipfs cid
root_cid = directory_data["path"]["/root"]

hashDict = {} # path, cid, arid
for path, cid in directory_data["path"].items():
    if path == "/root":
        continue
    arid = directory_data["data"][cid]
    hashDict[path] = [cid, arid]

# recover leaf cids
for path, hd in hashDict.items():
    print("Recovering", hd[-1])
    tx = Transaction(wallet, id=hd[-1])
    tx.get_data()
    n = len(tx.data)
    bytes_encoded = base64.b64encode( pre + encode(n) + tx.data + post + encode(n)).decode("latin1").replace("=", "")

    # make pb node
    node = {
        "Data": {"/": {'bytes': bytes_encoded}},
        "Links": []
    }

    b_data = json.dumps(node).encode('utf-8')
    # store the recovered data back to ipfs
    cid = client.dag.put(data=io.BytesIO(b_data))
    cid_v1 = make_cid(cid.as_json()["Cid"]["/"])
    cid_v0 = cid_v1.to_v0().encode().decode("utf-8")
    print(cid_v0, hd[0])
    assert(cid_v0 == hd[0])
    
    tsize = client.block.stat(cid_v0).get("Size")
    hashDict[path].append( tsize ) # update path dict to include Tsize
    print("------------------------")

# recover path dict
pathDict = {}
for path, cid in directory_data["path"].items():
    pathList = path[1:].split("/")
    if len(pathList) == 1:
        continue
    for i in range(len(pathList)-1):
        directory = "/" + "/".join(pathList[:i+1])
        subdirectory = pathList[i+1]
        if i not in pathDict.keys():
            pathDict[i] = {}
        if directory not in pathDict[i].keys():
            pathDict[i][directory] = [subdirectory]
        else:
            pathDict[i][directory].append(subdirectory)

# recover directory cids
depth = max(pathDict.keys())
for d in range(depth, -1, -1):
    directory_dict = pathDict[d]
    
    for path, names in directory_dict.items():

        tsize = 0

        # make pb node
        node = {
            "Data": {"/": {'bytes': "CAE"}},
            "Links": []
        }

        for name in names:
            full_path = path + "/" + name
            node['Links'].append({
                'Hash' : {'/' : hashDict[full_path][0]},
                'Name' : name,
                'Tsize': hashDict[full_path][-1]
            })
            tsize += hashDict[full_path][-1]

        # upload and get cid
        b_data = json.dumps(node).encode('utf-8')
        # store the recovered data back to ipfs
        cid = client.dag.put(data=io.BytesIO(b_data))
        cid_v1 = make_cid(cid.as_json()["Cid"]["/"])
        cid_v0 = cid_v1.to_v0().encode().decode("utf-8")
        
        # append this path into HashDict
        tsize += client.block.stat(cid_v0).get("Size")
        hashDict[path] = [cid_v0, "", tsize]

    print(node)

